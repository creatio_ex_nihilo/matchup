from matchups import *

ps = set()

for i in range(0, 5):
    ps.add(chr(65 + i))

for j in range(1):
    c = MatchupCreator(ps)
    t = MatchupTable(ps, c.sort_matchups())
    s = MatchupSave("test", t)

    print(*t.print_matchup_plan(), sep="")
    print(*t.print_table(), sep="")
    # s.save()
