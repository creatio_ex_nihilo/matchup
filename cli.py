from matchups import *

players = set()
strings = ["Please add a unique Player (type 'x' to stop)\n",
           "If you are ready to stop the script, type 'y'\n",
           "Player already known, please add a unique Player (type 'x' to stop)\n"]

current_string = strings[0]
run_script = True
while run_script:
    command = input(current_string)
    if command == 'x':
        c = MatchupCreator(players)
        t = MatchupTable(players, c.sort_matchups())
        print(*t.print_matchup_plan(), sep="")
        print(*t.print_table(), sep="")
        current_string = strings[1]
    elif current_string == strings[1] and command == 'y':
        run_script = False
    else:
        if command in players:
            current_string = strings[2]
        else:
            players.add(command)
            current_string = strings[0]
