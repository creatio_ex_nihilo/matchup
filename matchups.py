"""
Author: creatio_ex_nihilo
Description: This is a short script to create a matchup table for some kind of event,
where any player from the previous round isn't allowed to participate, so basically
to have pauses for them.
Version: 0.1.3

DISCLAIMER: this script won't finish if your player count is < 5, because there
is no logically possible answer for that

let's look at an example
A, B, C, D
A:B
A:C
A:D
B:C
B:D
C:D

let's start with
A:D
the only possible next step is
B:C

the remaining steps are
A:B -> no, because B just had a turn
A:C -> no, because C just had a turn
B:D -> no, because B just had a turn
C:D -> no, because C just had a turn
"""
import math
import os
import random


class MatchupCreator:
    # constant copy dict
    match = {'player_1': '', 'player_2': '', 'already_taken': False}

    def __init__(self, players):
        self.players = list(set(players))
        # checking after set() because we might lose some player duplicates
        if len(self.players) < 5:
            raise ValueError("there must be at least 5 players")
        self.matchups = self.create_matchups(self.players)

    '''
    Generates a matchup list from given players
    :param players: list given players 
    :returns: created matchup list
    '''
    def create_matchups(self, players):
        matchups = []
        # players - 1 because we don't need to create a matchup with just one player
        for i in range(len(players) - 1):
            current_player = players[i]
            for j in range(i + 1, len(players)):
                tmp = [current_player, players[j]]
                # only shuffled for the case in which there are sides which have to be random
                # (for example a ping pong table with on "good" side)
                random.shuffle(tmp)
                tmp_dict = MatchupCreator.match.copy()
                tmp_dict['player_1'] = tmp[0]
                tmp_dict['player_2'] = tmp[1]
                matchups.append(tmp_dict)
        return matchups

    def get_matchups(self):
        return self.matchups

    def sort_matchups(self):
        # constant for the loop max after which most of the time a possible sorting is found
        loop_max = 5
        smus = []
        found_one = False
        while not found_one:
            # (re)set 'already_taken'
            self.reset_taken_matchups()

            # offset (shuffle the matchups)
            random.shuffle(self.matchups)

            smus = self.try_to_find_matchups(loop_max)
            # only if all matchups are in the sorted matchup list
            if len(smus) == len(self.matchups):
                found_one = True
        return smus

    def try_to_find_matchups(self, loop_max):
        # offset (take one of the pre-shuffled matchups)
        self.matchups[0]['already_taken'] = True
        smus = [self.matchups[0]]
        # init cnt
        cnt = self.get_nontaken_matchups_len()

        current_matchup = smus[0]
        loop_cnt = 0
        # if loop_max is reached the last chosen matchup doesn't lead to a
        # possible next matchup, so start over
        while cnt != 0 and loop_max > loop_cnt:
            loop_cnt += 1
            for matchup in self.matchups:
                if not matchup['already_taken'] and self.cmp_matchups(current_matchup, matchup) == 1:
                    # choose matchup if none of the players participated in the previous one
                    matchup['already_taken'] = True
                    smus.append(matchup)
                    current_matchup = smus[-1]
                    # update cnt
                    cnt = self.get_nontaken_matchups_len()
        return smus

    def cmp_matchups(self, mu_1, mu_2):
        for player in mu_1.values():
            if player in mu_2.values():
                return 0
        return 1

    def reset_taken_matchups(self):
        for matchup in self.matchups:
            matchup['already_taken'] = False

    def get_nontaken_matchups_len(self):
        cnt = 0
        for matchup in self.matchups:
            if not matchup['already_taken']:
                cnt += 1
        return cnt


class MatchupTable:

    def __init__(self, players, matchups):
        self.players = list(set(players))
        self.matchups = matchups

    def print_matchup_plan(self):
        nr = 1
        lines = []
        max_len = self.calc_column_stuff()

        # get len of matchups, convert into string, get len of that string and convert to string
        # and you get the number of max digits for NR
        max_digit = len(str(len(self.matchups)))
        max_digit_with_stuff = '{:0' + str(max_digit) + '}'
        offset = max_digit + 2

        # constant
        magic_size_number = 13

        lines.append(
            self.create_column("-", [offset, 1], '-') +
            self.create_column("-", max_len, '-') +
            self.create_column("-", [magic_size_number, 5], '-') +
            self.create_column("-", max_len, '-') +
            '\n')

        lines.append(
            self.create_column("NR", [offset, 1], ' ') +
            self.create_column("PLAYER1", max_len) +
            self.create_column("P1  :  P2", [magic_size_number, 2]) +
            self.create_column("PLAYER2", max_len) +
            '\n')

        lines.append(
            self.create_column("-", [offset, 1], '-') +
            self.create_column("-", max_len, '-') +
            self.create_column("-", [magic_size_number, 5], '-') +
            self.create_column("-", max_len, '-') +
            '\n')

        for matchup in self.matchups:
            lines.append(self.create_column(max_digit_with_stuff.format(nr), [offset, 1], ' ') +
                         self.create_column(matchup["player_1"], max_len) +
                         self.create_column(":", [magic_size_number, 6]) +
                         self.create_column(matchup["player_2"], max_len) +
                         '\n')
            lines.append(self.create_column("-", [offset, 1], '-') +
                         self.create_column("-", max_len, '-') +
                         self.create_column("-", [magic_size_number, 5], '-') +
                         self.create_column("-", max_len, '-') +
                         '\n')
            nr += 1
        return lines

    def print_table(self):
        player_list = sorted(self.players)
        max_len = self.calc_column_stuff()

        num_rows = 2 * (len(player_list) + 1)
        num_columns = (len(player_list) + 1)

        num_games_len = len(str(len(player_list))) + 1
        num_games_len_with_stuff = '{}{:0' + str(num_games_len) + '}'

        rows = []
        cnt_row = ""

        for i in range(num_rows):
            for j in range(num_columns):
                # insert row separator every odd row
                if i % 2 == 1:
                    if j == 0:
                        cnt_row += self.create_column("-", max_len, '-')
                    else:
                        if j == num_columns - 1:
                            cnt_row += self.create_column("-", [7, 1], '-')
                        else:
                            cnt_row += self.create_column("-", [num_games_len + 7, 1], '-')
                else:
                    if j == 0 and i != 0:
                        # first column (except first row)
                        cnt_row += self.create_column(player_list[math.ceil(i / 2 - 1)], max_len, ' ')
                        # cnt_row += create_column(player_list[i-1], max_len, ' ')
                    elif i == 0 and j != 0:
                        # first row (except first column)
                        if j == num_columns - 1:
                            cnt_row += self.create_column("COUNT", [7, 1], ' ')
                        else:
                            cnt_row += self.create_column(num_games_len_with_stuff.format("GAME ", j),
                                                          [num_games_len + 7, 1], ' ')
                    elif i == 0 and j == 0:
                        # first row first column
                        cnt_row += self.create_column("NAME", max_len, ' ')
                    else:
                        if j == num_columns - 1:
                            cnt_row += self.create_column(" ", [7, 1], ' ')
                        else:
                            cnt_row += self.create_column(" ", [num_games_len + 7, 1], ' ')
            rows.append(cnt_row + '\n')
            cnt_row = ""
        return [rows[-1]] + rows

    @staticmethod
    def create_column(input, max_len, special_char=' '):
        all_padding = max_len[0] - len(input)
        back_padding = all_padding - max_len[1]
        output = "|"
        for i in range(max_len[1]):
            output += special_char
        output += input
        for i in range(back_padding):
            output += special_char
        output += "|"
        return output

    def calc_column_stuff(self):
        # find the longest player name
        max_player_len = len(max(self.players, key=len))
        # yes, magic number, bad stuff, but it's just the length of "Player1"
        if max_player_len <= 7:
            max_player_len = 7
        # constant
        column_len = max_player_len + 2
        # filler offset
        offset = math.ceil((column_len - max_player_len) / 2)
        # returns the length of the widest column
        return [offset * 2 + max_player_len, offset]


class MatchupSave:

    def __init__(self, filename, matchup_table):
        self.filename = filename
        self.matchup_table = matchup_table

    def save(self):
        with open(os.path.dirname(os.path.realpath(__file__)) + "\\" + self.filename + ".txt", 'w') as file:
            for line in self.matchup_table.print_matchup_plan():
                file.write(line)
            file.write('\n')
            for line2 in self.matchup_table.print_table():
                file.write(line2)